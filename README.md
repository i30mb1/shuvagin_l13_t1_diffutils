# Recycler View

## Tech-stack
* [ListAdapter](https://developer.android.com/reference/android/support/v7/recyclerview/extensions/ListAdapter) - convenience wrapper around AsyncListDiffer that implements Adapter common default behavior for item access and counting.
* [PrecomputedTextCompat](https://developer.android.com/reference/android/text/PrecomputedText?hl=en) - As per google measuring text can take up to 90% of the time required to set the text. to solve the problem google has introduce PrecomputedText
* [SwipeRefreshLayout](https://developer.android.com/reference/androidx/swiperefreshlayout/widget/SwipeRefreshLayout) - The SwipeRefreshLayout should be used whenever the user can refresh the contents of a view via a vertical swipe gesture.
* [ItemTouchHelper](https://developer.android.com/reference/android/support/v7/widget/helper/ItemTouchHelper) - This is a utility class to add swipe to dismiss and drag & drop support to RecyclerView.

![image](screen1.png)![image](screen2.png)

