package com.example.shuvagin_l17_t1

class Dota(var players: Int) {
    val name by lazy {
        "hello"
    }

    init {
        require(players == 10) { "should be 10 players" }
    }

    companion object {
        const val name = "i30mb1"
    }
}
