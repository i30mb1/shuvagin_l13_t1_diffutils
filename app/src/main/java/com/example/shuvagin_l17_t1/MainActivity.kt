package com.example.shuvagin_l17_t1

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.recyclerview.widget.*
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.api.load
import coil.transform.CircleCropTransformation
import coil.transform.Transformation
import com.commit451.coiltransformations.gpu.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    val adapter by lazy {
        MyListAdapter()
    }
    lateinit var list: MutableList<HouseCard>
    lateinit var recyclerView: RecyclerView
    var recentlyDeletedItem: HouseCard? = null
    var recentlyDeletedPosition: Int = 0
    val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
    val adjectives = arrayListOf(
        "Acclaimed",
        "Jubilant",
        "Accomplishe",
        "Keen",
        "Acrobatic",
        "Kooky",
        "Brave",
        "Lanky",
        "Bright",
        "Lazy",
        "Brilliant",
        "Limp",
        "Composed",
        "Luxurious",
        "Concerned",
        "Mediocre",
        "Concrete",
        "Mellow",
        "Conventional",
        "Miserable",
        "Delirious",
        "Nocturnal",
        "Demanding",
        "Organic",
        "Flustered",
        "Ornate",
        "Focused",
        "Ordinary",
        "Foolhardy",
        "Powerless",
        "Gregarious",
        "Practical",
        "Grim",
        "Precious",
        "Handsome",
        "Puzzled",
        "Handy",
        "Questionable",
        "Intelligent",
        " Quirky"
    )
    val advers = arrayListOf(
        "Angrily",
        "Meagerly",
        "Apathetically",
        "Methodically",
        "Amusingly",
        "Neglectfully",
        "Assertively",
        "Normally",
        "Begrudgingly",
        "Pointlessly",
        "Blissfully",
        "Quickly",
        "Cooly",
        "Rapidly",
        "Dutifully",
        "Rashly",
        "Eagerly",
        "Seriously",
        "Faintly",
        "Tactfully",
        "Frivolously",
        "Tragically",
        "Greedily",
        "Vacantly",
        "Hastily",
        "Vividly",
        "Intelligently",
        "Weirdly",
        "Kindly",
        "Youthfully",
        "Lazily",
        "Zealously"
    )
    val houseType = arrayListOf(
        "House",
        "Palace",
        "Domain",
        "Dome",
        "Appartment",
        "Box",
        "Condominium",
        "Mansion",
        "Residence",
        "Casle",
        "Cave"
    )
    val text =
        "A powerful fox known as the Nine-Tails attacks Konoha, the hidden leaf village in the Land of Fire, one of the Five Great Shinobi Countries in the Ninja World. In response, the leader of Konoha, the Fourth Hokage, seals the fox inside the body of his newborn son, Naruto Uzumaki, making Naruto a host of the beast;[e] this costs Naruto's father his life, and the Third Hokage returns from retirement to become leader of Konoha again. Naruto is often ridiculed by the Konoha villagers for being the host of the Nine-Tails. Because of a decree made by the Third Hokage forbidding anyone to mention these events, Naruto knows nothing about the Nine-Tails until 12 years later, when Mizuki, a renegade ninja, reveals the truth to Naruto. Naruto then defeats Mizuki in combat, earning the respect of his teacher Iruka Umino.[f]\n" + "\n" + "Shortly afterwards, Naruto becomes a ninja and joins with Sasuke Uchiha, against whom he often competes, and Sakura Haruno, on whom he has a crush, to form Team 7, under an experienced sensei, the elite ninja Kakashi Hatake. Like all the ninja teams from every village, Team 7 completes missions requested by the villagers, ranging from doing chores and being bodyguards to performing assassinations.\n" + "\n" + "After several missions, including a major one in the Land of Waves, Kakashi allows Team 7 to take a ninja exam, enabling them to advance to a higher rank and take on more difficult missions, known as Chunin Exams. During the exams, Orochimaru, a wanted criminal, invades Konoha and kills the Third Hokage for revenge. Jiraiya, one of the three legendary ninjas, declines the title of Fifth Hokage and searches with Naruto for Tsunade whom he chooses to become Fifth Hokage instead.\n" + "\n" + "During the search, it is revealed that Orochimaru wishes to train Sasuke because of his powerful genetic heritage, the Sharingan.[g] After Sasuke attempts and fails to kill his older brother Itachi[h] when he showed up in Konoha to kidnap Naruto, he joins Orochimaru, hoping to gain from him the strength needed to kill Itachi. The story takes a turn when Sasuke leaves the Konoha village and when Tsunade finds out, she sends a group of ninja, including Naruto, to retrieve Sasuke, but Naruto is unable to persuade or force him to come back. Naruto and Sakura do not give up on Sasuke: Naruto leaves Konoha to receive training from Jiraiya to prepare himself for the next time he encounters Sasuke, while Sakura becomes Tsunade's apprentice."
    lateinit var textMassive: List<String>
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDummyText()
        createList()
        setupRecyclerView()
    }

    private fun createDummyText() {
        textMassive =
            text.replace(".", " ").replace(",", " ").toLowerCase(Locale.getDefault()).split(" ")
        textMassive.forEach { it.trim() }
        textMassive.distinct()
    }

    private fun createRandomTitle(): String =
        "${adjectives.random()} ${advers.random()} ${houseType.random()}"

    private fun createRandomDescription(): String {
        return (1..20).map { i -> Random.nextInt(0, textMassive.size) }.map(textMassive::get)
            .joinToString(" ")
    }

    private fun createRandomRoof(): Drawable = getDrawable(R.drawable.ic_home_roof)!!.apply {
        setTint(getMatColor("A700"))
    }

    private fun createRandomBase(): Drawable = getDrawable(R.drawable.ic_home_base)!!.apply {
        setTint(Color.argb(255, Random.nextInt(256), Random.nextInt(256), Random.nextInt(256)))
    }

    private inline fun createRandomHouse(): HouseCard = HouseCard(
        createRandomTitle(),
        createRandomDescription(),
        createRandomRoof(),
        createRandomBase()
    )

    private fun createList() {
        list = MutableList(6) {
            createRandomHouse()
        }
    }

    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var staggeredGridLayoutManager: StaggeredGridLayoutManager
    private fun setupRecyclerView() {
        recyclerView = findViewById(R.id.rv_activity_main)
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        staggeredGridLayoutManager =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager =
            if (isLinearLayout) linearLayoutManager else staggeredGridLayoutManager
        recyclerView.adapter = adapter
        adapter.submitList(list)

        setOnTouchListener()
        setOnSwipeToRefresh()
    }

    private fun setOnSwipeToRefresh() {
        swipeRefreshLayout = findViewById(R.id.swipe_activity_main)
        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            adapter.submitList(arrayListOf())
            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                createList()
                adapter.submitList(list)
            }, 2000)
        }
    }

    private fun setOnTouchListener() {
        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val newList: MutableList<HouseCard> = (adapter.currentList).toMutableList()
                val adapterPosition = viewHolder.adapterPosition
                recentlyDeletedPosition = adapterPosition
                val item = newList[adapterPosition]
                recentlyDeletedItem = item
                newList.remove(item)
                list = newList
                adapter.submitList(newList)

                showUndoSnackBar("DELETED")
            }

            private val background = ColorDrawable(Color.RED)
            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
                val itemView = viewHolder.itemView
                val itemHeight = itemView.bottom - itemView.top
                val backgroundCornerOffset = 20
                if (dX > 0) { // Swiping to the right
                    background.setBounds(
                        itemView.left,
                        itemView.top + itemView.paddingTop,
                        itemView.left + dX.toInt() + itemView.paddingLeft,
                        itemView.bottom - itemView.paddingBottom
                    )
                    background.alpha = dX.toInt() / 5
                } else if (dX < 0) { // Swiping to the left
                    background.setBounds(
                        itemView.right + dX.toInt() - backgroundCornerOffset,
                        itemView.top + itemView.paddingTop,
                        itemView.right,
                        itemView.bottom - itemView.paddingBottom
                    )
                    background.alpha = 255 - dX.toInt() / 5
                } else { // view is unSwiped
                    background.setBounds(0, 0, 0, 0)
                }
                background.draw(c)
            }
        }).attachToRecyclerView(recyclerView)
    }

    private fun showUndoSnackBar(s: String) {
        Snackbar.make(rv_activity_main, s, Snackbar.LENGTH_LONG).setAction("UNDO") {
            undoDelete()
        }.show()
    }

    fun getMatColor(typeColor: String): Int {
        var returnColor = Color.BLACK
        val arrayId = resources.getIdentifier("mdcolor_$typeColor", "array", packageName)

        if (arrayId != 0) {
            val colors = resources.obtainTypedArray(arrayId)
            val index = (Math.random() * colors.length()).toInt()
            returnColor = colors.getColor(index, Color.BLACK)
            colors.recycle()
        }
        return returnColor
    }

    private fun undoDelete() {
        val newList: MutableList<HouseCard> = (adapter.currentList).toMutableList()
        recentlyDeletedItem?.let {
            newList.add(recentlyDeletedPosition, it)
            adapter.submitList(newList)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    internal var isLinearLayout = true
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_main_add -> {
                val newList = mutableListOf(createRandomHouse())
                newList.addAll(list)
                list = newList
                adapter.submitList(newList)
                val startPosition = 0
                recentlyDeletedPosition++
                recyclerView.smoothScrollToPosition(startPosition)
            }
            R.id.menu_main_change_layout -> {
                isLinearLayout = !isLinearLayout
                setupRecyclerView()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    data class HouseCard(
        var title: String,
        var description: String,
        var roof: Drawable,
        var base: Drawable
    )

    inner class MyListAdapter : ListAdapter<HouseCard, MyListAdapter.ViewHolder>(object :
        DiffUtil.ItemCallback<HouseCard>() {
        override fun areItemsTheSame(oldItem: HouseCard, newItem: HouseCard): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: HouseCard, newItem: HouseCard): Boolean {
            return true
        }
    }) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                if (isLinearLayout) R.layout.item_house_card else R.layout.item_house_card_grid,
                parent,
                false
            )
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = getItem(position)
            holder.set(item)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var ivPhotoRoof: ImageView = itemView.findViewById(R.id.iv_item_house_card_photo_roof)
            var ivPhotoBase: ImageView = itemView.findViewById(R.id.iv_item_house_card_photo_base)
            var ivDelete: ImageView = itemView.findViewById(R.id.iv_item_house_card_delete)
            var tvTitle: AppCompatTextView = itemView.findViewById(R.id.tv_item_house_card_title)
            var tvDescription: AppCompatTextView =
                itemView.findViewById(R.id.tv_item_house_card_description)

            fun set(item: HouseCard) {
                tvTitle.setTextFuture(
                    PrecomputedTextCompat.getTextFuture(
                        item.title,
                        tvTitle.textMetricsParamsCompat,
                        null
                    )
                )
                tvDescription.setTextFuture(
                    PrecomputedTextCompat.getTextFuture(
                        item.description,
                        tvDescription.textMetricsParamsCompat,
                        null
                    )
                )

                ivPhotoRoof.load(item.roof) {
                    transformations(CircleCropTransformation())
                }
                ivPhotoBase.load(item.base) {
                    transformations(CircleCropTransformation())
                }
            }

            fun getRandomTransformation(context: Context): Transformation {
                return arrayListOf<Transformation>(
                    KuwaharaFilterTransformation(context),
                    PixelationFilterTransformation(context),
                    SepiaFilterTransformation(context),
                    SketchFilterTransformation(context),
                    ToonFilterTransformation(context),
                    VignetteFilterTransformation(context)
                ).random()
            }
        }
    }
}
